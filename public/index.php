<?php
header('Content-Type: text/html; charset=UTF-8');
$user = 'u20239';
$pass = '3755350';
$db = new PDO('mysql:host=localhost;dbname=u20239', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
function is_abilites_right(array $abilities){
  $right_abilities = ['Immortal', 'Levitation', 'Walk through walls'];
  if(count($abilities) == 0) {
  return false;
  } else {
  foreach($abilities as $ability_key => $ability_value){
    if(!in_array($ability_value, $right_abilities))
     return false;
  }
  return true;
}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 1);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limb'] = !empty($_COOKIE['limb_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['contract'] = !empty($_COOKIE['contract_error']);
  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректное имя. Имя должно включать в себя только символы кириллицы      и английские буквы </div>';
  }
  if ($errors['email']) { 
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректный email. Email должен быть в формате: 
    example@domen.ru, допускаются цифры и \'_\' на месте "example"
    </div>';
  }
  if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректная дата. Дата должна быть в формате гггг-мм-дд</div>';
  }
  if ($errors['gender']) {
    setcookie('gender_error', '', 100000);
    $messages[] = '<div class="errormsg">Выберите пол.</div>';
  }
  if ($errors['limb']) {
    setcookie('limb_error', '', 100000);
    $messages[] = '<div class="errormsg">Выберите количество конечностей.</div>';
  }
   if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректная биография. Биография должна содержать от 5 символов
    (А-Я, а-я, A-Z, a-z,0-9, " ", _)
    </div>';
  }
  if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages[] = '<div class="errormsg">Выберите сверхспособности.</div>';
  }
   if ($errors['contract']) {
    setcookie('contract_error', '', 100000);
    $messages[] = '<div class="errormsg">Вы не поставили галочку.</div>';
  }
  $values = array();

  $values['fio'] = empty($_COOKIE['fio_value']) || 
  !preg_match('/^[A-Za-zА-Яа-я]+$/u',$_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];

  $values['email'] = empty($_COOKIE['email_value']) || 
  !preg_match('/^[A-Za-z_0-9]+@[A-Za-z]+\.[A-Za-z]+$/u',$_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  
  $values['date'] = empty($_COOKIE['date_value']) || 
  !preg_match('/^\d\d\d\d\-\d\d\-\d\d$/u',$_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];

  $values['gender'] = empty($_COOKIE['gender_value']) || 
  !preg_match('/^[MW]$/u',$_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];

  $values['limb'] = empty($_COOKIE['limb_value']) || 
  !preg_match('/^[1234]$/u',$_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];

  $values['abilities'] = empty($_COOKIE['abilities_value']) || 
  !is_abilites_right(json_decode($_COOKIE['abilities_value']))  ? '' : ($_COOKIE['abilities_value']);

  $values['bio'] = empty($_COOKIE['bio_value']) || 
  !preg_match('/^[A-Za-z_0-9А-Яа-я\n\r\s]{5,}$/u',$_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];

  $values['contract'] = empty($_COOKIE['contract_value']) ? '' : $_COOKIE['contract_value'];

  if (!in_array(1, $errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
        $md5pass = md5($_SESSION['pass']);
        $stmt = $db->prepare('SELECT name, email, date, gender, limb, abilities, bio, contract
                            FROM Users WHERE login = ? AND password = ?');
        $stmt->bindValue(1, $_SESSION['login'], PDO::PARAM_STR);
        $stmt->bindValue(2, $md5pass, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
        $values['fio'] = strip_tags($row['name']);
        $values['email'] = strip_tags($row['email']);
        $values['date'] = strip_tags($row['date']);
        $values['gender'] = strip_tags($row['gender']);
        $values['limb'] = strip_tags($row['limb']);
        $values['abilities'] = strip_tags($row['abilities']);
        $values['bio'] = strip_tags($row['bio']);
        $values['contract'] = strip_tags($row['contract']);
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    echo '<br> Вы можете <a href = "login.php"> выйти </a>';
  } 
 include('form.php');
  
}



// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
 $errors = FALSE;

  if (!preg_match('/^[A-Za-zА-Яа-я]+$/u', $_POST['fio'])) {
    setcookie('fio_error', '1', 0);
    $errors = TRUE;
  }  else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60 * 12);
  }


 if (!preg_match('/^[A-Za-z_0-9]+@[A-Za-z]+\.[A-Za-z]+$/', $_POST['email'])) {
  setcookie('email_error', '1', 0);
  $errors = TRUE;
 } else {
  setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
 }

if (!preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $_POST['date'])) {
  setcookie('date_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('date_value', $_POST['date'], time() + 12 * 30 * 24 * 60 * 60);
}

if(empty($_POST['gender']))
$_POST['gender']='';
if (!preg_match('/^[MW]$/', $_POST['gender'])) {
  setcookie('gender_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('gender_value', $_POST['gender'], time() + 12 * 30 * 24 * 60 * 60);
}                    
if (!preg_match('/^[A-Za-z_0-9А-Яа-я\n\r\s]{5,}$/u', $_POST['bio'])) {
  setcookie('bio_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
}
if(empty($_POST['limb']))
$_POST['limb']='';
if (!preg_match('/^[1234]$/', $_POST['limb'])) {
  setcookie('limb_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('limb_value', $_POST['limb'], time() + 12 * 30 * 24 * 60 * 60);
}

if(empty($_POST['abilities'])){
  setcookie('abilities_error', '1', 0);
  $errors=TRUE;
} else if (!is_abilites_right($_POST['abilities'])){
  setcookie('abilities_error', '1', 0);
  $errors=TRUE;
} else {
  setcookie('abilities_value', json_encode($_POST['abilities']), time() + 60 * 60 * 24 * 30 * 12);
}

if(empty($_POST['contract'])){
  setcookie('contract_error', '1', 0);
  $errors=TRUE;
} else {
  setcookie('contract_value', 'checked', time() + 60 * 60 * 24 * 30 * 12);
}

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
   header('Location: index.php');
    exit(); 
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limb_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('contract_error', '', 100000);
    
  }

  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');
  $user_abilities = json_encode($_POST['abilities']);
if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
       if(empty($_POST['csrf']) 
        || $_POST['csrf'] != substr($_POST['csrf'], 0, 4) . ":".md5(substr($_POST['csrf'], 0, 4) . $_SESSION['secret']))
        {
        exit();
        }
       
        try {
          $stmt = $db->prepare("UPDATE Users SET name = ?,  email = ?,  date = ?,  gender = ?,  
          limb= ?,  abilities = ?, bio = ?, contract = ? WHERE id = ?;");
          $stmt -> execute([ 
           $_POST['fio'], 
           $_POST['email'], 
           $_POST['date'], 
           $_POST['gender'], 
           $_POST['limb'], 
           $user_abilities,
           $_POST['bio'],
           'ok',
           $_SESSION['uid']
           ]);
        }
        catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
        }
  }else{
 $login = substr(uniqid(), 0 , 8);
 $pass = substr(md5(uniqid()), 0 , 8);
 setcookie('login', $login);
setcookie('pass', $pass); 
try {
  $stmt = $db->prepare("INSERT INTO Users (login, password,name, email, date, gender, limb, abilities, bio, contract) VALUES(:login, :pass, :name, :email, :date, :gender, :limb, :abilities, :bio, :contract);");
  $stmt -> execute(array(
  'login' => $login,
  'pass' => md5($pass),   
  'name'=>$_POST['fio'], 
  'email' => $_POST['email'], 
  'date' => $_POST['date'], 
  'gender' => $_POST['gender'], 
  'limb' => $_POST['limb'], 
  'abilities' => $user_abilities,
  'bio' => $_POST['bio'],
  'contract' => 'ok'));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
  }
  header('Location: index.php');

}
