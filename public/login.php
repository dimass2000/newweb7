<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  session_destroy();
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<?php 
  if(!empty($_COOKIE['enter_error'])){
    echo '<div class = "errormsg"> Неверный логин или пароль </div>';
    setcookie('enter_error', '', 1000);
  }
?>
<head> 
  <link href="style.css" rel="stylesheet">
</head>
<form action="" method="post">
  <p> Логин 
    <input name="login" />
  </p>
  <p>  Пароль
  <input name="pass" />
  </p>
  <input type="submit" value="Войти" />
</form>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $user = 'u20239';
  $pass = '3755350';
  $db = new PDO('mysql:host=localhost;dbname=u20239', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt = $db->prepare('SELECT * FROM Users WHERE login = ? AND password = ?;');
  $stmt->bindValue(1, $_POST['login'], PDO::PARAM_STR);
  $stmt->bindValue(2, md5($_POST['pass']), PDO::PARAM_STR);
  $stmt->execute();
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  if(!empty($rows)){
  $_SESSION['login'] = $_POST['login'];
  $_SESSION['pass'] = $_POST['pass'];
  $_SESSION['secret'] = substr(md5(uniqid()), 0, 7);
  $_SESSION['uid'] = $rows[0]['id'];
  header('Location: ./');
  } 
  else 
  {
    setcookie('enter_error', 1, 0);
    header('Location: login.php');
  }
  
}
