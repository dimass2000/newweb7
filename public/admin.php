<?php
$user = 'u20239';
$pass = '3755350';
$db = new PDO('mysql:host=localhost;dbname=u20239',$user, $pass, array(PDO::ATTR_PERSISTENT => true));
$query = $db->prepare('SELECT * FROM Admin WHERE login = ? AND pass = ?');
$query->execute([
  $_SERVER['PHP_AUTH_USER'],
  md5($_SERVER['PHP_AUTH_PW'])
]
);
$row = $query->fetchAll();

if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    empty($row)) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
} 

if($_SERVER['REQUEST_METHOD'] == 'GET'){

  include('table.php');

} else {
  $stmt = $db->prepare('DELETE FROM Users WHERE id = ?');
  $stmt->execute([$_POST['id']]);
  header('Location: admin.php');
}

?>
